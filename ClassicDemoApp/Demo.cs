﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassicDemoApp.Logging;

namespace ClassicDemoApp
{
    public class Demo
    {
        private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();

        public void Execute()
        {
            Logger.Info("execute start");

            Logger.Info("execute end");
        }
    }
}
