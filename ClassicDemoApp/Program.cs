﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassicDemoApp.Logging;
using Serilog;

namespace ClassicDemoApp
{
    class Program
    {
        private static ILog _logger;

        static void Main(string[] args) 
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.LiterateConsole()
                .CreateLogger();

            Log.Logger.Information("Serilog startup");

            _logger = LogProvider.GetCurrentClassLogger();
            _logger.Info("Startup (via LibLog interface)");

            var demo = new Demo();
            demo.Execute();

            Log.Logger.Information("exit app");
        }
    }
}
