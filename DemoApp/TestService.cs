﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibLog.Shared.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DemoApp
{
    public interface ITestService
    {
        void Run();
    }

    class TestService : ITestService
    {
        private static readonly ILog LibLogger = LogProvider.For<TestService>();

        private readonly ILogger<TestService> _logger;
        private readonly AppSettings _config;

        public TestService(ILogger<TestService> logger, 
            IOptions<AppSettings> config)
        {
            _logger = logger;
            _config = config.Value;
        }

        public void Run()
        {
            _logger.LogWarning($"Wow! We are now in the test service of: {_config.Title}");
            LibLogger.WarnFormat("Wow! We are now in the test service of: {Title}", _config.Title);
        }
    }
}
