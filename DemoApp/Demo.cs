﻿using LibLog.Shared.Logging;

namespace DemoApp
{
    public class Demo
    {
        //private static readonly ILog Logger = LogProvider.GetCurrentClassLogger();
        private static readonly ILog Logger = LogProvider.For<Demo>();

        public void Execute()
        {
            Logger.Info("execute start");

            Logger.Info("execute end");
        }
    }
}
