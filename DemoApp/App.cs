﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DemoApp
{
    public class App
    {
        private readonly ITestService _testService;
        private readonly ILogger<App> _logger;
        private readonly AppSettings _config;

        public App(
            ITestService testService,
            ILogger<App> logger, 
            IOptions<AppSettings> config    // notice injection syntax!
            )
        {
            _testService = testService;
            _logger = logger;
            _config = config.Value;         // use .Value!
        }

        public void Run()
        {
            //_logger.LogInformation($"This is a console application for asfdsaf");
            _logger.LogInformation($"This is a console application for {_config.Title}");
            _testService.Run();
            System.Console.ReadKey();
        }
    }
}
