﻿using System;
using System.IO;
using Autofac;
using Serilog;
using LibLog.Shared.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Serilog.Events;

namespace DemoApp
{
    class Program
    {
        // http://pioneercode.com/post/dependency-injection-logging-and-configuration-in-a-dot-net-core-console-app

        private static ILog _logger;
        public static IConfigurationRoot Configuration { get; set; }

        public static void Main(string[] args)
        {
            // configure Serilog/LibLog logging
            InitializeLogger();

            // create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // entry to run app
            serviceProvider.GetService<App>().Run();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            //// https://github.com/serilog/serilog-extensions-logging
            //// Serilog logging registration
            var loggerFactory = new LoggerFactory()
                .AddSerilog()
                //.AddConsole()
                .AddDebug()
                ;

            serviceCollection.AddSingleton(loggerFactory);

            //// for asp.net core web app: Ensure any buffered events are sent at shutdown
            //appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);

            //// add configured instance of logging
            //serviceCollection.AddSingleton(new LoggerFactory()
            //    .AddConsole()
            //    .AddDebug());

            // add logging
            serviceCollection.AddLogging();

            // build configuration
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("app-settings.json", false)
                .Build();

            serviceCollection.AddOptions();
            serviceCollection.Configure<AppSettings>(configuration.GetSection("Configuration"));

            // add services
            serviceCollection.AddTransient<ITestService, TestService>();

            // add app
            serviceCollection.AddTransient<App>();
        }

        private static void InitializeLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .Enrich.FromLogContext()            // ???
                .WriteTo.LiterateConsole()
                .WriteTo.Trace()
                .CreateLogger();

            Log.Logger.Information("Serilog initialized");

            _logger = LogProvider.For<Program>();
            _logger.Info("Startup (via LibLog interface)");
        }

        //static void Main(string[] args)
        //{
        //    Log.Logger = new LoggerConfiguration()
        //        .WriteTo.LiterateConsole()
        //        .CreateLogger();

        //    Log.Logger.Information("Serilog startup");

        //    //_logger = LogProvider.GetCurrentClassLogger();
        //    _logger = LogProvider.For<Program>();
        //    _logger.Info("Startup (via LibLog interface)");

        //    TestConfiguration();

        //    TestDependencyInjection();

        //    var demo = new Demo();
        //    demo.Execute();

        //    Log.Logger.Information("exit app");
        //}

        //private static void TestDependencyInjection()
        //{
        //    // https://joonasw.net/view/aspnet-core-di-deep-dive

        //    var builder = new ContainerBuilder();

        //    builder.RegisterInstance(Configuration).As<IConfigurationRoot>();
        //    // builder.Register
        //    // builder.Register

        //    // builder.RegisterType<TodayWriter>().As<IDateWriter>();
        //    var container = builder.Build();

        //    // IOptions<>

        //    using (var scope = container.BeginLifetimeScope())
        //    {
        //        var cfg = scope.Resolve<IConfigurationRoot>();
        //        Console.WriteLine($"XXoption1 = {Configuration["option1"]}");
        //        //writer.WriteDate();
        //    }
        //}

        //private static void TestConfiguration()
        //{
        //    // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration
        //    var builder = new ConfigurationBuilder()
        //        .SetBasePath(Directory.GetCurrentDirectory())
        //        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

        //    Configuration = builder.Build();

        //    Console.WriteLine($"option1 = {Configuration["option1"]}");
        //    Console.WriteLine($"option2 = {Configuration["option2"]}");
        //    Console.WriteLine(
        //        $"suboption1 = {Configuration["subsection:suboption1"]}");
        //    Console.WriteLine();

        //    Console.WriteLine("Wizards:");
        //    Console.Write($"{Configuration["wizards:0:Name"]}, ");
        //    Console.WriteLine($"age {Configuration["wizards:0:Age"]}");
        //    Console.Write($"{Configuration["wizards:1:Name"]}, ");
        //    Console.WriteLine($"age {Configuration["wizards:1:Age"]}");

        //    //var myOptionsConfiguration = Configuration.GetSection(nameof(MyOptions));
        //    var subSectionConfiguration = Configuration.GetSection("subsection");
        //    var subOpt1Value = subSectionConfiguration[nameof(MySubOptions.SubOption1)];
        //    var subOpt2Value = subSectionConfiguration[nameof(MySubOptions.SubOption2)];

        //    //// Configure JwtIssuerOptions
        //    //services.Configure<JwtIssuerOptions>(options =>
        //    //{
        //    //    options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
        //    //    options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
        //    //    options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
        //    //});
        //}
    }

    public class MyOptions
    {
        public MyOptions()
        {
            // Set default value.
            Option1 = "value1_from_ctor";
        }
        public string Option1 { get; set; }
        public int Option2 { get; set; } = 5;
    }

    public class MySubOptions
    {
        public MySubOptions()
        {
            // Set default values.
            SubOption1 = "value1_from_ctor";
            SubOption2 = 5;
        }
        public string SubOption1 { get; set; }
        public int SubOption2 { get; set; }
    }

}